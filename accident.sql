SET FOREIGN_KEY_CHECKS=0;
drop table if exists ImpactPoint;
drop table if exists VehicleType;
drop table if exists LicenseStatus;
drop table if exists Injury;
drop table if exists Race;
drop table if exists AgeGroup;
drop table if exists Role;
drop table if exists CrashType;
drop table if exists TimeOfDay;
drop table if exists DayOfWeek;
drop table if exists MannerOfCollision;
drop table if exists LandUseType;
drop table if exists OptionType;
drop table if exists Accident;
drop table if exists Location;
drop table if exists WeatherCondition;
drop table if exists Vehicle;
drop table if exists PersonInCar;
drop table if exists Driver;
drop table if exists Person;
SET FOREIGN_KEY_CHECKS=1;


-- ImpactPoint TABLE
CREATE TABLE ImpactPoint (impactPointID int not null, description varchar(255), primary key (impactPointID));
INSERT INTO ImpactPoint 
VALUES 
(1, "Non-Collision"),
(2, "Front"),
(3, "Right Side"),
(4, "Rear"),
(5, "Left Side"),
(6, "Other"),
(7, "Unknown");


-- Vehicle type Tale
CREATE TABLE VehicleType (vehicleTypeID int not null, description varchar(255), primary key (vehicleTypeID));
INSERT INTO VehicleType 
VALUES 
(1, "Passenger Car"),
(2, "Light Truck - Pickup"),
(3, "Light Truck - Utility"),
(4, "Light Truck - Van"),
(5, "Light Truck - Other"),
(6, "Large Truck"),
(7, "Motorcycle"),
(8, "Bus"),
(9, "Other/Unknown");

-- LicenseStatus table
CREATE TABLE LicenseStatus (licenseStatusID int not null, description varchar(255), primary key (licenseStatusID));
INSERT INTO LicenseStatus 
VALUES 
(1, "Valid"),
(2, "Invalid"),
(3, "Unknown"),
(4, "Not Applicable");

-- Injury table
CREATE TABLE Injury (injuryTypeID int not null, description varchar(255), primary key (injuryTypeID));
INSERT INTO Injury 
VALUES 
(1, "Fatal (FARS)"),
(2, "Incapacitating Injured Estimate (GES)"),
(3, "Nonincapacitating Injured Estimate (GES)"),
(4, "Other Injured Estimate (GES)"),
(5, "Not Injured Estimate (GES)"),
(6, "Survivor in Fatal Crash (FARS)"),
(7, "Other/Unknown (Including Estimated Fatalities) (GES)");

-- Race table
CREATE TABLE Race (raceID int not null, description varchar(255), primary key (raceID));
INSERT INTO Race 
VALUES 
(0, "Not Applicable or Not Available"),
(1, "White"),
(2, "Black"),
(3, "American Indian"),
(4, "Asian"),
(5, "Pacific Islander"),
(6, "Mixed Race"),
(7, "All Other Races"),
(8, "Unknown");

-- AgeGroup Table
CREATE TABLE AgeGroup (ageGroupID int not null, description varchar(255), primary key (ageGroupID));
INSERT INTO AgeGroup 
VALUES 
(1, "0-3"),
(2, "4-7"),
(3, "8-12"),
(4, "13-15"),
(5, "16-20"),
(6, "21-24"),
(7, "25-34"),
(8, "35-44"),
(9, "45-54"),
(10, "55-64"),
(11, "65-74"),
(12, "75+"),
(13, "Unknown");

-- Role table 
CREATE TABLE Role (roleID int not null, description varchar(255), primary key (roleID));
INSERT INTO Role 
VALUES 
(1, "Driver"),
(2, "Occupant"),
(3, "Pedestrian"),
(4, "Pedalcyclist"),
(5, "Other/Unknown NonOccupants");

-- Crash Table
CREATE TABLE CrashType (crashTypeID int not null, description varchar(255), primary key (crashTypeID));
INSERT INTO CrashType 
VALUES 
(1, "Single-Vehicle Crash"),
(2, "Two-Vehicle Crash"),
(3, "More Than Two-Vehicle Crash");

-- TimeOfDay table
CREATE TABLE TimeOfDay (timeOfDayID int not null, description varchar(255), primary key (timeOfDayID));
INSERT INTO TimeOfDay 
VALUES 
(1, "Daytime"),
(2, "Nighttime"),
(3, "Unknown");

-- DayOfWeek table
CREATE TABLE DayOfWeek (dayOfWeekID int not null, description varchar(255), primary key (dayOfWeekID));
INSERT INTO DayOfWeek 
VALUES 
(1, "Weekday"),
(2, "Weekend"),
(3, "Unknown");

-- MannerOfCollision TABLE
CREATE TABLE MannerOfCollision (mannerOfCollisionID int not null, description varchar(255), primary key (mannerOfCollisionID));
INSERT INTO MannerOfCollision 
VALUES 
(1, "Not Collision with Motor Vehicle in Transport"),
(2, "Rear-End"),
(3, "Head-On"),
(4, "Angle"),
(5, "Sideswipe"),
(6, "Other"),
(7, "Unknown");

-- LandUseType TABLE
CREATE TABLE LandUseType (landUseTypeID int not null, description varchar(255), primary key (landUseTypeID));
INSERT INTO LandUseType 
VALUES 
(1, "Rural"),
(2, "Urban"),
(3, "Unknown");

-- OptionType table
CREATE TABLE OptionType (optionTypeID int not null, description varchar(255), primary key (optionTypeID));
INSERT INTO OptionType 
VALUES 
(1, "Yes"),
(2, "No"),
(3, "Other"),
(4, "Unknown");

-- Location table
CREATE TABLE Location (
	locationID int not null auto_increment, 
	streetNumber int, 
	street varchar(255), 
	city varchar(255), 
	county varchar(255),
	state varchar(255), 
	zipCode varchar(255), 
	country varchar(255),

	primary key (locationID)
);

-- Accident table
-- most accidents have a location, so put locationID in this table as a foreign key
-- many accidents can have the same location (esp. since FARS data only has state + country)
CREATE TABLE Accident (
	accidentID int not null, 
	year int,
	crashTypeID int, 
	timeOfDayID int, 
	dayOfWeekID int, 
	mannerOfCollisionID int, 
	landUseTypeID int, 
	atIntersectionID int, 
	atInterstateID int, 
	atJunctionID int, 
	isHitAndRun TINYINT(1), 
	isSpeeding TINYINT(1),
	locationID int,

	primary key (accidentID), 
	foreign key (crashTypeID) references CrashType(crashTypeID) on delete cascade on update cascade,
	foreign key (timeOfDayID) references TimeOfDay(timeOfDayID) on delete cascade on update cascade,
	foreign key (dayOfWeekID) references DayOfWeek(dayOfWeekID) on delete cascade on update cascade,
	foreign key (mannerOfCollisionID) references MannerOfCollision(mannerOfCollisionID) on delete cascade on update cascade,
	foreign key (landUseTypeID) references LandUseType(landUseTypeID) on delete cascade on update cascade,
	foreign key (atIntersectionID) references OptionType(optionTypeID) on delete cascade on update cascade,
	foreign key (atInterstateID) references OptionType(optionTypeID) on delete cascade on update cascade,
	foreign key (atJunctionID) references OptionType(optionTypeID) on delete cascade on update cascade,
	foreign key (locationID) references Location(locationID) on delete cascade on update cascade
);

-- Condition Table
-- since it will be very rare for the exact same weather condition to appear twice, we will allow duplicate entries
-- have accidentID in this table
CREATE TABLE WeatherCondition (
	accidentID int not null, 
	temperature float, 
	humidity float, 
	weather varchar(255), 
	visibility float, 

	primary key (accidentID),
	foreign key (accidentID) references Accident(accidentID) on delete cascade on update cascade
);


-- ####################################################################################################################
-- Vehicle Table
CREATE TABLE Vehicle (
	accidentID int not null,
	vehicleID int not null,
	isRollover TINYINT(1),
	initialImpactPointID int,
	vehicleTypeID int,
	licenseStatusID int,

	constraint PK_Vehicle primary key (accidentID, vehicleID),

	foreign key (accidentID) references Accident(accidentID) on delete cascade on update cascade,
	foreign key (initialImpactPointID) references ImpactPoint(impactPointID) on delete cascade on update cascade,
	foreign key (vehicleTypeID) references VehicleType(vehicleTypeID) on delete cascade on update cascade,
	foreign key (licenseStatusID) references LicenseStatus(licenseStatusID) on delete cascade on update cascade
);

-- ####################################################################################################################
-- Person table
-- connected to Accident via accidentID
CREATE TABLE Person (
	accidentID int not null,
	personID int not null,
	ageGroupID int,
	roleID int,
	raceID int,
	injuryTypeID int,

	constraint PK_Person primary key (accidentID, personID),

	foreign key (roleID) references Role(roleID) on delete cascade on update cascade,
	foreign key (ageGroupID) references AgeGroup(ageGroupID) on delete cascade on update cascade,
	foreign key (raceID) references Race(raceID) on delete cascade on update cascade,
	foreign key (injuryTypeID) references Injury(injuryTypeID) on delete cascade on update cascade,
	foreign key (accidentID) references Accident(accidentID) on delete cascade on update cascade
);


-- PeopleInCar table
-- Has foreign key to vehicle, allowing users to query who is in which car
-- Disjoint from Person
CREATE TABLE PersonInCar (
	accidentID int not null,
	personID int not null,
	vehicleID int,
	ejectionID int,
	isProtectedID int,

	constraint PK_PersonInCar primary key (accidentID, personID),

	foreign key (ejectionID) references OptionType(optionTypeID) on delete cascade on update cascade,
	foreign key (isProtectedID) references OptionType(optionTypeID) on delete cascade on update cascade,

	constraint FK_PersonInCar foreign key (accidentID, personID) references Person(accidentID, personID) on delete cascade on update cascade,
	constraint FK_Car foreign key (accidentID, vehicleID) references Vehicle(accidentID, vehicleID) on delete cascade on update cascade
);

-- Driver Table
CREATE TABLE Driver (
	accidentID int not null,
	personID int not null,
	isDriverDrunkID int,
	isDriverDrowsy TINYINT(1),
	isDriverDistracted TINYINT(1),

	constraint PK_Driver primary key (accidentID, personID),

	foreign key (isDriverDrunkID) references OptionType(optionTypeID) on delete cascade on update cascade,

	constraint FK_Driver foreign key (accidentID, personID) references PersonInCar(accidentID, personID) on delete cascade on update cascade
);