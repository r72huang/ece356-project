import csv
import pandas as pd
import datetime 
from datetime import datetime

acc_aux = "data/ACC_AUX.CSV"
veh_aux = "data/VEH_AUX.CSV"
per_aux = "data/PER_AUX.CSV"
set1 = "data/set1.csv"
county_codes = "data/county_codes.xlsx"


fAll = open("load-everything.sql", "w")

######################################## Dataset 1 FILE ########################################
locationID = 1 #keep track of the auto incremented location ID
fs = open("load-set1.sql", "w")

with open(set1) as s1_file:
	s1_reader = csv.DictReader(s1_file, delimiter = ',')


	#Note: When parsing data, we will come across accident and location info in the same row
	#For each row, if the location is unique, we need to create an insert statement to add an entry to Location table
	#We must be careful not to insert duplicate entries
	#So we will need to keep track of what location has which ID
	#This way, when given a location, we can search to see if it has already been entered
	#In which case we just need to retrieve the ID, which will be Accident.locationID

	location = "NULL" #this goes into accident table (it is the fk)
	locationIDMapping = {} #keep track of locations that have already been inserted to prevent duplicate locations


	i = 10000 # limit 10,000 inserts... there is 1 mil + records in this file.

	for row in s1_reader: #loop through the raw data

		if i<= 0: #limit i inserts
			break
		i-=1

		#we will concat all location info as the "key" in our dictionary. 
		key = row['Number']+row['Street']+row['City']+row['County']+row['State']+row['Zipcode']+row['Country'] #to check if location has already been inserted

		if locationIDMapping.get(key) is not None: #already exists in dictionary (and location table)

			location = locationIDMapping.get(key) #just get the ID. do not need to insert new entry

		else: #need to add new entry in location table + dictionary

			location = locationID 
			locationIDMapping[key] = locationID #add to dictionary
			locationID += 1 #locationID in the db auto-increments, we need to keep track of this

			#extract data
			streetNumber = row['Number'] if row['Number']!='' else 'NULL'
			street = row['Street']
			city = row['City']
			county = row['County']
			state = row['State']
			zipCode = row['Zipcode']
			country = row['Country']

			#generate insert statement
			insert_loc = (f'insert into Location(streetNumber,street,city,county,state,zipCode,country) values('
				f'{streetNumber},'
				f'\'{street}\','
				f'\'{city}\','
				f'\'{county}\','
				f'\'{state}\','
				f'\'{zipCode}\','
				f'\'{country}\');'
			)

			#write to file
			fs.write(insert_loc+'\n\r')
			fAll.write(insert_loc+'\n\r')


		#extract other data
		date = row['Start_Time'].split(' ')[0]
		dt = datetime.strptime(date, '%Y-%m-%d')
		hour = int(row['Start_Time'].split(' ')[1].split(':')[0])

		accidentID = row['ID'][2:] #remove A-
		year = date.split('-')[0]
		crashType = "NULL"
		timeOfDay = 1 if hour >= 6 and hour <= 18 else 2 #match datetime into an option in TimeOfDay table
		dayOfWeek = 1 if dt.weekday() < 5 else 2 #match datetime into an option in DayOfWeek table
		mannerOfCollision = "NULL"
		landUseType = "NULL"
		atIntersection = "NULL"
		atInterstate = "NULL"
		atJunction = 1 if row['Junction'] == 'True' else 2
		isHitAndRun = "NULL"
		isSpeeding = "NULL"

		#generate insert statement
		insert_acc = (f'insert into Accident values('
			f'{accidentID},'
			f'{year},'
			f'{crashType},'
			f'{timeOfDay},'
			f'{dayOfWeek},'
			f'{mannerOfCollision},'
			f'{landUseType},'
			f'{atIntersection},'
			f'{atInterstate},'
			f'{atJunction},'
			f'{isHitAndRun},'
			f'{isSpeeding},'
			f'{location});'
		)
		
		#write to file
		fs.write(insert_acc+'\n\r')
		fAll.write(insert_acc+'\n\r')

		#extract info for WeatherCondition
		temperature = row['Temperature(F)'] if row['Temperature(F)'] != '' else 'NULL'
		humidity = row['Humidity(%)'] if row['Humidity(%)'] != '' else 'NULL'
		weather = '\''+row['Weather_Condition']+'\'' if row['Weather_Condition'] != '' else 'NULL'
		visibility = row['Visibility(mi)'] if row['Visibility(mi)'] != '' else 'NULL'

		if temperature != 'NULL' or humidity != 'NULL' or weather != 'NULL' or visibility != 'NULL':
			insert_wc = (f'insert into WeatherCondition values('
				f'{accidentID},' 
				f'{temperature},' 
				f'{humidity},'
				f'{weather},'
				f'{visibility});' 
			)

			fs.write(insert_wc+'\n\r')
			fAll.write(insert_wc+'\n\r')


######################################## ACCIDENT FILE ########################################

fa = open("load-acc.sql", "w")

df = pd.read_excel(county_codes, sheet_name = "GeoLocation_UnitedStates")
with open(acc_aux) as acc_file:

	acc_reader = csv.DictReader(acc_file, delimiter = ',')
	locationIDMapping = {} #map state + county to a locationID
	for row in acc_reader:

		#Note: Again, we will need to keep track of what locations have already been added
		#we can start with a blank dictionary because FARS data only contains State, County, Country while the first set had everything
		#we will concat state and county and check if they already exist in the dictionary. If not, add it to both dictionary and Location table.

		location = "NULL" #this goes into accident table
		state = "NULL"
		county = "NULL"
		country = '\'US\''
		key = row['STATE']+row['COUNTY']

		if locationIDMapping.get(key) is not None: #already exists in dictionary (and location table)
			location = locationIDMapping.get(key)

		else: #need to add new enty in location table + dictionary
			
			location = locationID
			locationIDMapping[key] = locationID
			locationID += 1

			#The county and state codes come from the file GeoLocation_UnitedStates.
			#We need to search this file to find the string values for the state and county.

			stateDF = df.loc[df['State Code'] == int(row['STATE'])] #get the state name (if exists)
			if not stateDF.empty:
				state = '\''+stateDF.iloc[0]['State Name']+'\''
			
			if int(row['COUNTY']) > 0 and int(row['COUNTY']) < 997:
				countyDF = df.loc[df['County Code'] == int(row['COUNTY'])] #get the county name (if exists)
				if not countyDF.empty:
					county = '\''+countyDF.iloc[0]['County Name']+'\''

			insert_loc = (f'insert into Location(county, state, country) values('
				f'{county},'
				f'{state},'
				f'{country});'
			)
			fa.write(insert_loc+'\n\r')
			fAll.write(insert_loc+'\n\r')

		#extract accident attributes (refer to manual/Translation.xlsx)
		accidentID = row['ST_CASE']
		year = row['YEAR']
		crashType = row['A_CT']
		timeOfDay = row['A_TOD']
		dayOfWeek = row['A_DOW']
		mannerOfCollision = row['A_MANCOL']
		landUseType = row['A_RU']
		atIntersection = row['A_INTSEC']
		atInterstate = row['A_INTER']
		atJunction = row['A_JUNC']
		isHitAndRun = '1' if row['A_HR'] == '1' else '0'
		isSpeeding = '1' if row['A_SPCRA'] == '1' else '0'

		insert_acc = (f'insert into Accident values('
			f'{accidentID},'
			f'{year},'
			f'{crashType},'
			f'{timeOfDay},'
			f'{dayOfWeek},'
			f'{mannerOfCollision},'
			f'{landUseType},'
			f'{atIntersection},'
			f'{atInterstate},'
			f'{atJunction},'
			f'{isHitAndRun},'
			f'{isSpeeding},'
			f'{location});'
		)
		
		fa.write(insert_acc+'\n\r')
		fAll.write(insert_acc+'\n\r')

fa.close()


######################################## VEHICLE FILE ########################################

fv = open("load-veh.sql", "w")

#Note: The attributes isDriverDrowsy (A_DRDRO) and isDriverDistracted (A_DRDIS) is in the vehicle file, not person file.
#So, we need to keep a mapping of vehicleID to these values.
#when we generate SQL commands for the Person table using PER_AUX.csv, we can reference this mapping.

vehicleDriverMapping = {} #vehicleID:[isDriverDrowsy, isDriverDistracted]
with open(veh_aux) as veh_file:
	veh_reader = csv.DictReader(veh_file, delimiter = ',')
	for row in veh_reader:

		key = row['ST_CASE']+row['VEH_NO'] #VEH_NO does not uniquely identify the vehicle. Use it in conjunction with ST_CASE.
		distracted = '1' if row['A_DRDIS'] == '1' else '0'
		drowsy = '1' if row['A_DRDRO'] == '1' else '0'
		vehicleDriverMapping[key] = [drowsy,distracted] #add to mapping

		#extract vehicle data
		accidentID = row['ST_CASE']
		vehicleID = row['VEH_NO']
		isRollover = '1' if row['A_VROLL'] == '1' else '0'
		initialImpactPoint = row['A_IMP1']
		vehicleType = row['A_BODY']
		licenseStatus = row['A_LIC_S']

		insert_veh = (f'insert into Vehicle values('
			f'{accidentID},'
			f'{vehicleID},'
			f'{isRollover},'
			f'{initialImpactPoint},'
			f'{vehicleType},'
			f'{licenseStatus});'
		)

		fv.write(insert_veh+'\n\r')
		fAll.write(insert_veh+'\n\r')
fv.close()


######################################## PERSON FILE ########################################


fp = open("load-per.sql", "w")
fo = open("load-occ.sql", "w")
fd = open("load-driv.sql", "w")

with open(per_aux) as per_file:

	per_reader = csv.DictReader(per_file, delimiter = ',')

	for row in per_reader:
		#Extract Person attributes
		accidentID = row['ST_CASE']
		personID = row['PER_NO']+row['VEH_NO']
		ageGroup = row['A_AGE3']
		role = row['A_PTYPE']
		race = row['A_RCAT']
		injuryType = row['A_PERINJ']

		insert_per = (f'insert into Person values('
			f'{accidentID},'
			f'{personID},'
			f'{ageGroup},'
			f'{role},'
			f'{race},'
			f'{injuryType});'
		)

		fp.write(insert_per+'\n\r')
		fAll.write(insert_per+'\n\r')


		if row['A_PTYPE']=='1' or row['A_PTYPE']=='2': #driver (1) or occupant(2)

			#extract PersonInCar attributes
			vehicleID = row['VEH_NO']
			ejection = row['A_EJECT']
			isProtected = row['A_REST']

			insert_occ = (f'insert into PersonInCar values('
				f'{accidentID},'
				f'{personID},'
				f'{vehicleID},'
				f'{ejection},'
				f'{isProtected});'
			)

			fo.write(insert_occ+'\n\r')
			fAll.write(insert_occ+'\n\r') 


		if row['A_PTYPE']=='1': #driver
			isDriverDrunk = 'NULL' 

			#extract Driver attributes

			#we do not want to create a seperate table with values in A_ALCTES.
			#we want to use OptionType (Yes, No, Other, Unknown)
			#So we need to map it manually
			#For ex, if A_ALCTES = No Alcohol, isDriverDrunk should be No

			if row['A_ALCTES']=='1': #No Alcohol
				isDriverDrunk = '2' #No
			elif row['A_ALCTES']=='2': #Positive BAC
				isDriverDrunk = '1' #Yes
			elif row['A_ALCTES']=='3': #Not tested
				isDriverDrunk = '3' #Other
			elif row['A_ALCTES']=='4' or row['A_ALCTES']=='5': #Unknown
				isDriverDrunk = '4' #Unknown

			#Use the dictionary mapping vehicle to isDriverDrowsy/isDriverDistracted
			isDriverDrowsy = vehicleDriverMapping.get(row['ST_CASE']+row['VEH_NO'])[0]
			isDriverDistracted = vehicleDriverMapping.get(row['ST_CASE']+row['VEH_NO'])[1]

			insert_driv = (f'insert into Driver values('
				f'{accidentID},'
				f'{personID},'
				f'{isDriverDrunk},'
				f'{isDriverDrowsy},'
				f'{isDriverDistracted});'
			)

			fd.write(insert_driv+'\n\r')
			fAll.write(insert_driv+'\n\r') 

fp.close()
fo.close()
fd.close()
fAll.close()


