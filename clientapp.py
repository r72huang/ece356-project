import sys
import pymysql
from prettytable import PrettyTable
from datetime import datetime


db = pymysql.connect(host="marmoset04.shoshin.uwaterloo.ca", user="r72huang", passwd="Password@123", db="db356_r72huang")
cur = db.cursor()


#Displays data in a readable table format while allowing forward paging.
#query: The query to be executed
#isSelection: True/False, should the user be selecting a value returned by the query?
def display_data(query, isSelection):
	cur.execute(query)

	#get all attributes and append it to the header row of the table
	tableCols = []
	for cd in cur.description:
		tableCols.append(cd[0])

	pt = PrettyTable(tableCols) #make a table from the header rows

	idx = 1 #from row num
	num_rows_shown = 0 #to row num

	allrows = cur.fetchall() #get results of query

	#result set empty
	if len(allrows) == 0:
		print("No results.")

	#loop through results
	for row in allrows:
	    pt.add_row(row) #add row to table
	    num_rows_shown +=1

	    #if we have added 5 rows to the table, or if there are no more rows to add
	    if (num_rows_shown - idx == 4) or num_rows_shown == len(allrows): 

	    	#print the table
	    	print("Showing "+str(idx)+" to "+str(num_rows_shown)+" of " + str(len(allrows)) + " rows.")
	    	print(pt)

	    	#if there is no more to show and the user is not required to select a row, return
	    	#if the user is required to make a selection, wait for them to choose
	    	if num_rows_shown == len(allrows):
	    		if isSelection == False:
	    			return
	    		else:
	    			try:
	    				retVal = int(input("Please enter the "+tableCols[0] + ": "))
	    				return retVal
	    			except:
	    				print("Not a valid input.")
	    				return -1

	    	#if there are more rows to show, allow the user to go to the next page or exit the query
	    	#if they are required to make a selection, give them the option to do so too.
	    	else:
	    		prompt = ""
	    		if isSelection == False:
	    			prompt = "Press n to view next page. Press e to exit query: "
	    		else:
	    			prompt = "Press n to view next page. Press e to exit query. Otherwise, enter the "+tableCols[0]+": "
		    	nextOrQuit = input(prompt)
		    	if nextOrQuit == "e":
		    		return
		    	elif nextOrQuit != "n":
		    		try:
		    			retVal = int(nextOrQuit)
		    			return retVal
		    		except ValueError:
		    			print("Not a valid input.")

	    	idx = num_rows_shown+1
	    	pt.clear_rows()


#Allows the user to query the Accidents table
def query_accidents():
	print("The following queries are available.")
	print("\t1 - Accidents involving drunk drivers")
	print("\t2 - Accidents involving distracted drivers")
	print("\t3 - Accidents involving drowsy drivers")
	print("\t4 - Accidents involving invalid licenses")
	print("\t5 - Accidents involving fatal injuries")
	print("\t6 - Number of accidents during the day vs at night")
	print("\t7 - Number of accidents during weekdays vs weekends")
	print("\t8 - Number of accidents on rural vs urban land")
	print("\t9 - Impact of weather conditions on accidents")
	print("\t10 - Search accidents by ID")

	#have the user choose a query
	option = 0
	try:
		option = int(input("Please enter a number: "))
	except ValueError:
		option = -1

	query = ""

	#the columns we will show (translate to human readable with keyword 'as')
	accident_cols = ("Accident.accidentID, "
		"Accident.year, "
		"CrashType.description as 'Crash Type', "
		"TimeOfDay.description as 'Time of Day', "
		"DayOfWeek.description as 'Day of Week', " 
		"MannerOfCollision.description as 'Manner of Collision', "
		"LandUseType.description as 'Land Use Type', "
		"o1.description as 'At Intersection', "
		"o2.description as 'At Interstate', "
		"o3.description as 'At Junction', "
		"IF(Accident.isHitAndRun=0, 'No', 'Yes') as 'Hit and Run', "
		"IF(Accident.isSpeeding=0, 'No', 'Yes') as 'Speeding', "
		"Accident.locationID "
		)

	#get human readable string values instead of IDs with left joins
	join = ("left join CrashType using (crashTypeID) "
		"left join TimeOfDay using (timeOfDayID) "
		"left join DayOfWeek using (dayOfWeekID) "
		"left join MannerOfCollision using (mannerOfCollisionID) "
		"left join LandUseType using (landUseTypeID) "
		"left join OptionType o1 on Accident.atIntersectionID = o1.optionTypeID "
		"left join OptionType o2 on Accident.atInterstateID = o2.optionTypeID "
		"left join OptionType o3 on Accident.atJunctionID = o3.optionTypeID"
		)

	#make the query
	if option == 1:
		query = "select " + accident_cols + "from Accident "+join+ " where accidentID in (select accidentID from Driver where isDriverDrunkID = 1);"
	elif option == 2:
		query = "select " + accident_cols + "from Accident "+join+ " where accidentID in (select accidentID from Driver where isDriverDistracted = true);"
	elif option == 3:
		query = "select " + accident_cols + "from Accident "+join+ " where accidentID in (select accidentID from Driver where isDriverDrowsy = true)"
	elif option == 4:
		query = "select " + accident_cols + "from Accident "+join+ " where accidentID in (select accidentID from Vehicle where licenseStatusID = 2)"
	elif option == 5:
		query = "select " + accident_cols + "from Accident "+join+ " where accidentID in (select accidentID from Person where injuryTypeID = 1)"
	elif option == 6:
		query = "select (select count(*) from Accident where timeOfDayID = 1) as num_day, (select count(*) from Accident where timeOfDayID=2) as num_night;"
	elif option == 7:
		query = "select (select count(*) from Accident where dayOfWeekID = 1) as num_weekday, (select count(*) from Accident where timeOfDayID=2) as num_weekend;"
	elif option == 8:
		query = "select (select count(*) from Accident where landUseTypeID = 1) as num_rural, (select count(*) from Accident where timeOfDayID=2) as num_urban;"
	elif option == 9:
		query = "select accidentID, temperature, humidity, weather, visibility from Accident left join WeatherCondition using (accidentID)"
	elif option == 10:
		accidentID = input("Enter the accidentID you want to search for: ")
		query = "select " + accident_cols + "from Accident "+join+ " where accidentID = " + accidentID
	else:
		print("Sorry, that is not a valid input.")
		return
		

	display_data(query, False) #execute the query and show the data


#Allows the user to query the Person/PersonInCar/Driver table
def query_people():
	print("The following queries are available.")
	print("\t1 - All age ranges of people involved in accidents")
	print("\t2 - Driver condition vs crash type")
	print("\t3 - Age vs accident involvement")
	print("\t4 - Race vs accident involvement")
	print("\t5 - Impact of protection Usage (Helmets, seatbelts) on injury fatality")
	print("\t6 - All Drivers")
	print("\t7 - All Occupants")
	print("\t8 - All NonOccupants")
	print("\t9 - Search people by ID")
	print("\t10 - Search people by accidentID")

	#have the user choose a query
	option = 0
	try:
		option = int(input("Please enter a number: "))
	except ValueError:
		option = -1

	#the columns we will show (translate to human readable with keyword 'as')
	people_cols = ("Person.accidentID, concat(Person.accidentID, Person.personID) as 'personID', "
		"AgeGroup.description as 'Age Group', "
		"Race.description as 'Race', "
		"Injury.description as 'Injury Type'"
		)

	#for use in query 6 & 7: PeopleInCar attributes are applicable to occupants and drivers
	personInCar_cols = people_cols + (", PersonInCar.vehicleID,"
		"o1.description as 'Ejected',"
		"o2.description as 'Protected'"
		)

	#for use in query 6: Drivers also need attributes from Driver table
	driver_cols = personInCar_cols + (", o3.description as Drunk,"
		"IF(Driver.isDriverDrowsy=0, 'No', 'Yes') as 'Drowsy', "
		"IF(Driver.isDriverDistracted=0, 'No', 'Yes') as 'Distracted'"
		)

	#get human readable string values instead of IDs with left joins
	join = ("left join AgeGroup using (ageGroupID) "
		"left join Race using (raceID) "
		"left join Injury using (injuryTypeID) "
		)

	#for use in query 6 & 7: also translate attributes in PersonInCar table
	join_personInCar = join + ("left join OptionType o1 on PersonInCar.ejectionID = o1.optionTypeID "
		"left join OptionType o2 on PersonInCar.isProtectedID = o2.optionTypeID "
		)

	#for use in query 6: also translate attributes in Driver table
	join_driver = join_personInCar + ("left join OptionType o3 on Driver.isDriverDrunkID = o3.optionTypeID ")

	query = ""

	#make the query
	if option == 1:
		query = "select concat(accidentID, personID) as personID, ag.description as ageRange from Person left join AgeGroup ag using (ageGroupID);"
	elif option == 2:
		query = """select isDriverDrowsy, isDriverDistracted, ot.description as isDriverDrunk,
            ct.description as crashSeverity from Driver
            left join OptionType ot on optionTypeID=isDriverDrunkID
            left join Accident using (accidentID)
            left join CrashType ct using (crashTypeID)"""
	elif option == 3:
		query = "select ageGroupID, description as 'Age Group', count(ageGroupID) as 'num people' from Person left join AgeGroup using (ageGroupID) group by ageGroupID;"
	elif option == 4:
		query = "select raceID, description as 'Race', count(raceID) as 'num people' from Person left join Race using (raceID) group by raceID;"
	elif option == 5:
		query = """select Injury.description as 'Injury type', 
	(select count(*) from PersonInCar left join Person using (accidentID, personID) where Person.injuryTypeID = Injury.injuryTypeID and isProtectedID = 1) as 'Number Protected',
	(select count(*) from PersonInCar left join Person using (accidentID, personID) where Person.injuryTypeID = Injury.injuryTypeID and isProtectedID = 2) as 'Number Unprotected'
	from Injury;"""

	elif option == 6:
		query = "select " + driver_cols + " from Driver left join PersonInCar using (accidentID, personID) left join Person using (accidentID, personID) "+join_driver +";"
	elif option == 7:
		query = "select " + personInCar_cols + " from PersonInCar left join Person using (accidentID, personID) "+join_personInCar +"where roleID = 2;"
	elif option == 8:
		query = "select " + people_cols + " from Person "+join +"where roleID = 3 or roleID = 4;"
	elif option == 9:
		personID = input("Enter the personID you want to search for: ")
		query = "select " + people_cols + "from Person "+join+ " where concat(accidentID,personID) = " + personID
	elif option == 10:
		accidentID = input("Enter the accidentID you want to search for: ")
		query = "select " + people_cols + "from Person "+join+ " where accidentID = " + accidentID
	else:
		print("Sorry, that is not a valid input.")
		return
	
	display_data(query, False) #execute the query and show the data


def query_vehicles():
	print("The following queries are available.")
	print("\t1 - Unlicensed vehicles")
	print("\t2 - Vehicles that rolled over")
	print("\t3 - Search vehicle by ID")

	#have the user choose a query
	option = 0
	try:
		option = int(input("Please enter a number: "))
	except ValueError:
		option = -1

	query = ""

	#the columns we will show (translate to human readable with keyword 'as')
	vehicle_cols = ("accidentID, concat(accidentID,vehicleID) as 'vehicleID', IF(isRollover=0, 'No', 'Yes') as 'Rollover', "
		"ImpactPoint.description as 'Initial Impact Point', "
		"VehicleType.description as 'Vehicle Type', "
		"LicenseStatus.description as 'License Status' "
		)

	#get human readable string values instead of IDs with left joins
	join = (
		"left join ImpactPoint on (Vehicle.initialImpactPointID = ImpactPoint.impactPointID) "
		"left join VehicleType using (vehicleTypeID) "
		"left join LicenseStatus using (licenseStatusID) "
		)

	#make the query
	if option == 1:
		query = "select " + vehicle_cols + "from Vehicle "+join + "where licenseStatusID = 2;"
	elif option == 2:
		query = "select " + vehicle_cols + "from Vehicle "+join + "where isRollover = 1;"
	elif option == 3:
		vehicleID = input("Enter the vehicleID you want to search for: ")
		query = "select " + vehicle_cols + "from Vehicle "+join+ " where concat(accidentID,vehicleID) = " + vehicleID
	else:
		print("Sorry, that is not a valid input.")
		return

	display_data(query, False) #execute the query and show the data


#Insert into accident
def add_accident():

	#Ask user for accidentID
	#If user leaves blank, generate one by selecting the largest accidentID + 1
	accidentID = input("What is the accident ID? Leave blank to autofill: ")
	if len(accidentID) == 0:
		cur.execute("select max(accidentID) from Accident;")
		accidentID = int(cur.fetchone()[0]) + 1
		
	#ask user for other data
	year = input("What year did the accident occur? Leave blank if unknown: ")
	if len(year) == 0:
		year = "NULL"

	print("What is the crash type?")
	crashTypeID = display_data("select * from CrashType;", True)
	if crashTypeID == -1: return 

	print("\nDid the accident occur during daytime or nighttime?")
	timeOfDayID = display_data("select * from TimeOfDay;", True)
	if timeOfDayID == -1: return 

	print("\nDid the accident occur during a weekday or weekend?")
	dayOfWeekID = display_data("select * from DayOfWeek;", True)
	if dayOfWeekID == -1: return 

	print("\nWhat is the manner of collision?")
	mannerOfCollisionID = display_data("select * from MannerOfCollision;", True)
	if mannerOfCollisionID == -1: return 

	print("\nDid the accident occur on rural or urban lands?")
	landUseTypeID = display_data("select * from LandUseType;", True)
	if landUseTypeID == -1: return 

	print("\nDid the accident occur at an intersection?")
	atIntersectionID = display_data("select * from OptionType;", True)
	if atIntersectionID == -1: return 

	print("\nDid the accident occur at an interstate?")
	atInterstateID = display_data("select * from OptionType;", True)
	if atInterstateID == -1: return 

	print("\nDid the accident occur at a Junction?")
	atJunctionID = display_data("select * from OptionType;", True)
	if atJunctionID == -1: return 
	
	isHitAndRun = input("\nWas the accident a hit and run? Enter 0 for no and 1 for yes: ")
	
	isSpeeding = input("Did the accident involve speeding? Enter 0 for no and 1 for yes: ")

	print("\nWhere did the accident occur?")
	locationID = display_data("select * from Location;", True)
	if locationID == -1: return 

	#build the sql command
	sql = (f'insert into Accident values('
			f'{accidentID},'
			f'{year},'
			f'{crashTypeID},'
			f'{timeOfDayID},'
			f'{dayOfWeekID},'
			f'{mannerOfCollisionID},'
			f'{landUseTypeID},'
			f'{atIntersectionID},'
			f'{atInterstateID},'
			f'{atJunctionID},'
			f'{isHitAndRun},'
			f'{isSpeeding},'
			f'{locationID}); '
		)

	#execute the command
	try:
		cur.execute(sql)
		db.commit()
		print("Accident successfully added! Your accidentID is "+str(accidentID))
	except pymysql.Error as e:
		print("Unable to enter data - an error occured.")
		print(e)
		return

#Insert into Person/PersonInCar/Driver
def add_person():

	#Ask user for accidentID
	#If user leaves blank, output error (we need an accidentID to uniquely identify person)
	accidentID = input("What is the accident ID? ")
	if len(accidentID) == 0:
		print("Invalid input. Please create an accident for this person if you haven't already.")
		return

	#Ask user for personID
	#If user leaves blank, generate one by selecting the largest personID + 1
	personID = input("What is the personID? Leave blank to autofill: ")
	if len(personID) == 0:
		query = "select max(personID) from Person where accidentID = " + accidentID + ";"
		cur.execute(query)
		personID = 0
		relatedPeople = cur.fetchone()[0]
		if relatedPeople != None:
			personID = int(relatedPeople) + 1

	#ask other questions
	print("Which age group?")
	ageGroupID = display_data("select * from AgeGroup;", True)
	if ageGroupID == -1: return 

	print("What role?")
	roleID = display_data("select * from Role;", True)
	if roleID == -1: return 

	print("What race?")
	raceID = display_data("select * from Race;", True)
	if raceID == -1: return 

	print("What injury type?")
	injuryTypeID = display_data("select * from Injury;", True)
	if injuryTypeID == -1: return 

	#build SQL commands
	sql = (f'insert into Person values('
			f'{accidentID},'
			f'{personID},'
			f'{ageGroupID},'
			f'{roleID},'
			f'{raceID},'
			f'{injuryTypeID}); '
		)

	sql_pic = ""
	sql_driv = ""

	#if the role is driver or occupant, we need to add to PersonInCar
	if roleID == 1 or roleID == 2: 

		#prompt the user for vehicleID - if blank, output error (user should be entering concat(accidentID, vehicleID))
		vehicleID = input("What is the vehicle ID? ")
		if len(vehicleID) == 0:
			print("Invalid input. Please create a vehicle for this person if you haven't already.")
			return
		cur.execute("select vehicleID from Vehicle where concat(accidentID, vehicleID) = " + vehicleID +";")
		vres = cur.fetchone()
		if vres is not None:
			vehicleID = int(vres[0])
		else:
			print("Invalid vehicleID")
			return

		#ask other questions
		print("Was the person ejected?")
		ejectionID = display_data("select * from OptionType;", True)
		if ejectionID == -1: return 

		print("Was the person protected (wearing seatbelt or helmet)?")
		isProtectedID = display_data("select * from OptionType;", True)
		if isProtectedID == -1: return 

		#build SQL command
		sql_pic = (f'insert into PersonInCar values('
			f'{accidentID},'
			f'{personID},'
			f'{vehicleID},'
			f'{ejectionID},'
			f'{isProtectedID}); '
		)

	#if the role is Driver, we need to insert into Driver table
	if roleID == 1: 
		#ask relevant questions
		print("Was the driver drunk?")
		isDriverDrunkID = display_data("select * from OptionType;", True)
		if isDriverDrunkID == -1: return 

		isDriverDrowsy = 0
		isDriverDistracted = 0
		try:
			isDriverDrowsy = int(input("Was the driver drowsy? Enter 1 for yes and 0 for no: "))
			isDriverDistracted = int(input("Was the driver distracted? Enter 1 for yes and 0 for no: "))
		except ValueError:
			print("Not a valid input.")
			return

		#build SQL command
		sql_driv =  (f'insert into Driver values('
			f'{accidentID},'
			f'{personID},'
			f'{isDriverDrunkID},'
			f'{isDriverDrowsy},'
			f'{isDriverDistracted}); '
		)

	#execute sql command(s)
	try:
		cur.execute(sql)

		if len(sql_pic) > 0:
			cur.execute(sql_pic)

		if len(sql_driv) > 0:
			cur.execute(sql_driv)

		db.commit()
		print("Person successfully added! Your personID is "+ str(accidentID) + str(personID))
	except pymysql.Error as e:
		print("Unable to enter data - an error occured.")
		print(e)
		return


def add_vehicle():
	#Ask user for accidentID
	#If user leaves blank, output error (we need an accidentID to uniquely identify vehicle)
	accidentID = input("What is the accident ID? ")
	if len(accidentID) == 0:
		print("Invalid input. Please create an accident for this vehicle if you haven't already.")
		return

	#Ask user for vehicleID
	#If user leaves blank, generate one by selecting the largest vehicleID + 1
	vehicleID = input("What is the vehicleID? Leave blank to autofill: ")
	if len(vehicleID) == 0:
		query = "select max(vehicleID) from Vehicle where accidentID = " + accidentID + ";"
		cur.execute(query)
		vehicleID = 0
		relatedCars = cur.fetchone()[0]
		if relatedCars != None:
			vehicleID = int(relatedCars) + 1

	#ask relevant questions
	isRollover = 0
	try:
		isRollover = int(input("Did the vehicle roll over? Enter 1 for yes and 0 for no: "))
	except ValueError:
		print("Not a valid input.")
		return

	print("What was the initial impact point?")
	initialImpactPointID = display_data("select * from ImpactPoint;", True)
	if initialImpactPointID == -1: return 

	print("What is the vehicle type?")
	vehicleTypeID = display_data("select * from VehicleType;", True)
	if vehicleTypeID == -1: return 

	print("What is the license status?")
	licenseStatusID = display_data("select * from LicenseStatus;", True)
	if licenseStatusID == -1: return 

	#build SQL command
	sql = (f'insert into Vehicle values('
		f'{accidentID},'
		f'{vehicleID},'
		f'{isRollover},'
		f'{initialImpactPointID},'
		f'{vehicleTypeID},'
		f'{licenseStatusID}); '
	)

	#Execute command
	try:
		cur.execute(sql)
		db.commit()
		print("Vehicle successfully added! Your vehicleID is "+ str(accidentID) + str(vehicleID))
	except pymysql.Error as e:
		print("Unable to enter data - an error occured.")
		print(e)
		return

#Modify rows in the Accident table
def modify_accident():

	#which row to modify
	accidentID = input("Please enter the ID of the accident you'd like to modify: ")
	
	print("The following attributes are available.")
	print("\t1 - Year")
	print("\t2 - Crash Type")
	print("\t3 - Time of Day")
	print("\t4 - Day Of Week")
	print("\t5 - Manner Of Collision")
	print("\t6 - Land Use Type")
	print("\t7 - At Intersection")
	print("\t8 - At Interstate")
	print("\t9 - At Junction")
	print("\t10 - Hit And Run")
	print("\t11 - Speeding")
	print("\t12 - locationID")

	#ask user which attribute to modify
	c = 0
	try:
		c = int(input("Which attribute would you like to modify? "))
	except ValueError:
		print("Invalid input")
		return

	#depending on which attribute the user chose, show the corresponding values
	val = ""
	cname = ""
	if c == 1: 
		val = input("Enter the value: ")
		cname = "year"
	elif c == 2: 
		val = display_data("select * from CrashType;", True)
		cname = "crashTypeID"
	elif c == 3: 
		val = display_data("select * from TimeOfDay;", True)
		cname = "timeOfDayID"
	elif c == 4: 
		val = display_data("select * from DayOfWeek;", True)
		cname = "dayOfWeekID"
	elif c == 5: 
		val = display_data("select * from MannerOfCollision;", True)
		cname = "mannerOfCollisionID"
	elif c == 6: 
		val = display_data("select * from LandUseType;", True)
		cname = "landUseTypeID"
	elif c == 7: 
		val = display_data("select * from OptionType;", True)
		cname = "atIntersectionID"
	elif c == 8: 
		val = display_data("select * from OptionType;", True)
		cname = "atInterstateID"
	elif c == 9: 
		val = display_data("select * from OptionType;", True)
		cname = "atJunctionID"
	elif c == 10: 
		val = input("Enter 0 for no and 1 for yes: ")
		cname = "isHitAndRun"
	elif c == 11: 
		val = input("Enter 0 for no and 1 for yes: ")
		cname = "isSpeeding"
	elif c == 12: 
		val = display_data("select * from Location;", True)
		cname = "locationID"

	#build and execute sql command
	sql = "update Accident set "+cname+"="+str(val)+" where accidentID="+accidentID+";"

	try:
		cur.execute(sql)
		db.commit()
		print("Accident successfully modified!")
	except pymysql.Error as e:
		print("Unable to modify accident - an error occured.")
		print(e)
		return

#modify Person table
#will not implement modifying PersonInCar and Driver.
def modify_person(): 
	personID = input("Please enter the ID of the person you'd like to modify: ") #this ID is the accident+personID

	print("The following attributes are available.")
	print("\t1 - Age Group")
	print("\t2 - Race")
	print("\t3 - Injury Type")

	#ask user which attribute to modify
	c = 0
	try:
		c = int(input("Which attribute would you like to modify? "))
	except ValueError:
		print("Invalid input")
		return

	#depending on which attribute the user chose, show the corresponding values
	val = ""
	cname = ""
	if c == 1: 
		val = display_data("select * from AgeGroup;", True)
		cname = "ageGroupID"
	elif c == 2: 
		val = display_data("select * from Race;", True)
		cname = "raceID"
	elif c == 3: 
		val = display_data("select * from InjuryType;", True)
		cname = "injuryTypeID"

	#build and execute sql command
	sql = "update Person set "+cname+"="+str(val)+" where concat(accidentID, personID)="+personID+";"

	try:
		cur.execute(sql)
		db.commit()
		print("Person successfully modified!")
	except pymysql.Error as e:
		print("Unable to modify person - an error occured.")
		print(e)
		return


def modify_vehicle():
	vehicleID = input("Please enter the ID of the vehicle you'd like to modify: ") #this ID is accident+vehicleID

	print("The following attributes are available.")
	print("\t1 - Rollover")
	print("\t2 - Initial Impact Point")
	print("\t3 - Vehicle Type")
	print("\t4 - License Status")

	#ask user which attribute to modify
	c = 0
	try:
		c = int(input("Which attribute would you like to modify? "))
	except ValueError:
		print("Invalid input")
		return

	#depending on which attribute the user chose, show the corresponding values
	val = ""
	cname = ""
	if c == 1: 
		val = input("Enter 0 for no and 1 for yes: ")
		cname = "isRollover"
	elif c == 2: 
		val = display_data("select * from ImpactPoint;", True)
		cname = "initialImpactPointID"
	elif c == 3: 
		val = display_data("select * from VehicleType;", True)
		cname = "vehicleTypeID"
	elif c == 4: 
		val = display_data("select * from LicenseStatus;", True)
		cname = "licenseStatusID"

	#build and execute sql command
	sql = "update Vehicle set "+cname+"="+str(val)+" where concat(accidentID, vehicleID)="+vehicleID+";"

	try:
		cur.execute(sql)
		db.commit()
		print("Vehicle successfully modified!")
	except pymysql.Error as e:
		print("Unable to modify vehicle - an error occured.")
		print(e)
		return


###################################### MAIN #########################################

print("Welcome to accidents client app!")
print("What would you like to do today?")

cont = 1
while(cont == 1):
	print("\t1 - Query accidents \n\t2 - Add an accident \n\t3 - Modify an existing accident")
	print("\t4 - Query people \n\t5 - Add a person \n\t6 - Modify an existing person")
	print("\t7 - Query vehicles \n\t8 - Add a vehicle \n\t9 - Modify an existing vehicle")
	option = 0
	try:
		option = int(input("Please enter a number: "))
	except ValueError:
		option = -1

	if option == 1:
		query_accidents()
	elif option == 2:
		add_accident()
	elif option == 3:
		modify_accident()
	elif option == 4:
		query_people()
	elif option == 5:
		add_person()
	elif option == 6:
		modify_person()
	elif option == 7:
		query_vehicles()
	elif option == 8:
		add_vehicle()
	elif option == 9:
		modify_vehicle()
	else:
		print("Sorry, that is not a valid input.")

	print("Would you like to continue using the client app or quit?")
	print("\t1: continue \n\t2: quit")
	try:
		cont = int(input("Please enter a number: "))
	except ValueError:
		cont = -1

print("Goodbye.")
db.close()


